package ch.ti8m.academy.digitalgarage.course.p1_streams;

import ch.ti8m.academy.digitalgarage.course.util.Employee;
import com.google.common.collect.Iterables;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ch.ti8m.academy.digitalgarage.course.p1_streams.Exercise3.employees;

@SuppressWarnings("ALL")
public class Exercise3_Solution {

    public static void main(String[] args) {

        //TODO 1: The employee (name) with biggest salary?
        Stream.of(employees)
                .max(Comparator.comparing(Employee::getSalary))
                .ifPresent(employee -> System.out.println(String.format("Employee with biggest salary is (simple): %s", employee)));

        //Or, more verbose:
        Stream.of(employees)
                .max((e1, e2) -> e1.getSalary().compareTo(e2.getSalary()))
                .ifPresent(employee -> System.out.println(String.format("Employee with biggest salary is (verbose): %s", employee)));

        //Or, with findFirst
        Stream.of(employees)
                .sorted(Comparator.comparing(Employee::getSalary).reversed())
                .findFirst()
                .ifPresent(employee -> System.out.println(String.format("Employee with biggest salary is (findFirst): %s", employee)));

        //TODO 2: The average salary in Looney Tunes Inc.
        Stream.of(employees)
                .mapToDouble(Employee::getSalary)
                .average()
                .ifPresent(avg -> System.out.println(String.format("Average salary: %.2f", avg)));


        //TODO 3: How many unique words are contained in the name of the employees?
        long uniqueWords = Stream.of(employees)
                .map(employee -> employee.getName().split(" "))
                .flatMap(Arrays::stream)
                .distinct()
                .count();
        System.out.println(String.format("Unique words: %d", uniqueWords));


        //TODO 4': Employee index: Create a map with key = <first letter of name>, value = list(<Employee>)
        Map<Character, List<Employee>> employeeIndex1 = Stream.of(employees)
                .collect(Collectors.groupingBy(e -> e.getName().charAt(0)));
        System.out.println(String.format("Employee index: %s", employeeIndex1));

        //TODO 4": Employee index: Create a map with key = <first letter of last name>, value = list(<Employee>)
        Map<Character, List<Employee>> employeeIndex2 = Stream.of(employees)
                .collect(Collectors.groupingBy(Exercise3_Solution::getLastNameFirstChar));
        System.out.println(String.format("Employee index (by last name): %s", employeeIndex2));


        //TODO 5: Looney Tunes Inc. must pay 10% of each salary as taxes. Which is the company's overhead?
        Double overhead = Stream.of(employees)
                .map(e -> e.getSalary() * 0.1)
                .reduce(0.0, (s1, s2) -> s1 + s2);
        System.out.println(String.format("Overhead: %.2f", overhead));


        //
        System.out.println(String.format("Total salaries: %.2f", Stream.of(employees).mapToDouble(Employee::getSalary).sum()));
    }

    private static char getLastNameFirstChar(Employee e) {
        return Iterables.getLast(Arrays.asList(e.getName().split(" "))).charAt(0);
    }

}
