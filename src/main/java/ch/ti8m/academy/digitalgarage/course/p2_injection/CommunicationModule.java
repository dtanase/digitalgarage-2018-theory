package ch.ti8m.academy.digitalgarage.course.p2_injection;

import com.google.inject.AbstractModule;

public class CommunicationModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(Messenger.class).to(MessengerImpl.class);
    }

}
