package ch.ti8m.academy.digitalgarage.course.p1_lambda;

import java.util.function.Function;

public class Example2 {


    public static void main(String[] args) {

        Function<Integer, Integer> specialIncrement = x -> { //It looks like a function, it feels like a function, but has no name

            if(x % 2 == 0) {
                return  x + 2;
            } else {
                return x + 1;
            }

        };

        Integer result = specialIncrement.apply(10);

        System.out.println(result);


    }

}
