package ch.ti8m.academy.digitalgarage.course.p1_streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Example1 {

    public static void main(String[] args) {

        List<String> source = Arrays.asList("a1", "a2", "b1", "c2", "c1");

        List<String> result = source
                .stream()
                .filter(s -> s.startsWith("c"))
                .map(String::toUpperCase)
                .sorted()
                .collect(Collectors.toList());

        System.out.println(result);

    }

}
