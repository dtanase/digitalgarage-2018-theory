package ch.ti8m.academy.digitalgarage.course.util;

public enum Status {

    OK, WARNING, ERROR
}
