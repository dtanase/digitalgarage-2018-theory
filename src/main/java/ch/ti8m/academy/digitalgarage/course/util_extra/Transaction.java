package ch.ti8m.academy.digitalgarage.course.util_extra;

import ch.ti8m.academy.digitalgarage.course.util.Status;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public class Transaction {

    private Long id;

    private LocalDate date;

    private Status status;

    private BigDecimal value;

    private String bookingText;

    private Long customerId;

    private String creditAccount;

    private String debitAccount;

    private String country;

    private List<String> attributes;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getBookingText() {
        return bookingText;
    }

    public void setBookingText(String bookingText) {
        this.bookingText = bookingText;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(String creditAccount) {
        this.creditAccount = creditAccount;
    }

    public String getDebitAccount() {
        return debitAccount;
    }

    public void setDebitAccount(String debitAccount) {
        this.debitAccount = debitAccount;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


    public List<String> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }


    public static final class TransactionBuilder {
        private Long id;
        private LocalDate date;
        private Status status;
        private BigDecimal value;
        private String bookingText;
        private Long customerId;
        private String creditAccount;
        private String debitAccount;
        private String country;
        private List<String> attributes;

        private TransactionBuilder() {
        }

        public static TransactionBuilder aTransaction() {
            return new TransactionBuilder();
        }

        public TransactionBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public TransactionBuilder date(LocalDate date) {
            this.date = date;
            return this;
        }

        public TransactionBuilder status(Status status) {
            this.status = status;
            return this;
        }

        public TransactionBuilder value(BigDecimal value) {
            this.value = value;
            return this;
        }

        public TransactionBuilder bookingText(String bookingText) {
            this.bookingText = bookingText;
            return this;
        }

        public TransactionBuilder customerId(Long customerId) {
            this.customerId = customerId;
            return this;
        }

        public TransactionBuilder creditAccount(String creditAccount) {
            this.creditAccount = creditAccount;
            return this;
        }

        public TransactionBuilder debitAccount(String debitAccount) {
            this.debitAccount = debitAccount;
            return this;
        }

        public TransactionBuilder country(String country) {
            this.country = country;
            return this;
        }

        public TransactionBuilder attributes(List<String> attributes) {
            this.attributes = attributes;
            return this;
        }

        public Transaction build() {
            Transaction transaction = new Transaction();
            transaction.setId(id);
            transaction.setDate(date);
            transaction.setStatus(status);
            transaction.setValue(value);
            transaction.setBookingText(bookingText);
            transaction.setCustomerId(customerId);
            transaction.setCreditAccount(creditAccount);
            transaction.setDebitAccount(debitAccount);
            transaction.setCountry(country);
            transaction.setAttributes(attributes);
            return transaction;
        }
    }


}