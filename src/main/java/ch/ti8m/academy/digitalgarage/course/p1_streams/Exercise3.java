package ch.ti8m.academy.digitalgarage.course.p1_streams;

import ch.ti8m.academy.digitalgarage.course.util.Employee;

public class Exercise3 {

    static Employee[] employees = {
            new Employee(1, "Porky Pig", 1000),
            new Employee(2, "Elmer Fudd", 100),
            new Employee(3, "Daffy Duck", 200),
            new Employee(4, "Bugs Bunny", 300),
            new Employee(5, "Pepé Le Pew", 300),
            new Employee(6, "Charlie Dog", 500),
            new Employee(7, "Yosemite Sam", 600),
            new Employee(8, "Melissa Duck", 400),
    };

    public static void main(String[] args) {

        //TODO 1: The employee (name) with biggest salary?


        //TODO 2: The average salary in Looney Tunes Inc.


        //TODO 3: How many unique words are contained in the name of the employees?


        //TODO 4': Employee index: Create a map with key = <first letter of name>, value = list(<Employee>)


        //TODO 4": Employee index: Create a map with key = <first letter of last name>, value = list(<Employee>)


        //TODO 5: Looney Tunes Inc. must pay 10% of each salary as taxes. Which is the company's overhead?

    }

}
