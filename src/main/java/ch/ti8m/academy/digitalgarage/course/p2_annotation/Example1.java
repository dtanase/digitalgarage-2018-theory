package ch.ti8m.academy.digitalgarage.course.p2_annotation;

import ch.ti8m.academy.digitalgarage.course.util.Status;
import ch.ti8m.academy.digitalgarage.course.util.Transaction;

import java.math.BigDecimal;

/**
 * Based on: https://dzone.com/articles/creating-custom-annotations-in-java
 */

public class Example1 {

    public static void main(String[] args) throws JsonSerializeException {

        Transaction transaction = Transaction.builder()
                .id(1L)
                .status(Status.WARNING)
                .value(BigDecimal.valueOf(100.00))
                .attribute("attr1")
                .build();

        JsonSerializer serializer = new JsonSerializer();
        String json = serializer.serialize(transaction);

        System.out.println(json);

    }

}
