package ch.ti8m.academy.digitalgarage.course.p1_lambda;

import java.util.function.Function;

public class Example3 {


    public static void main(String[] args) {

        final Integer incrementer = 2; //Must be final or effectively final

        Function<Integer, Integer> specialIncrement = x -> {

            Integer result; //Not shared outside the scope of the function

            if(x % 2 == 0) {
                result = x + incrementer;
            } else {
                result = x + 1;
            }

            return result;

        };

        Integer result = specialIncrement.apply(10);

        System.out.println(result);


    }

}
