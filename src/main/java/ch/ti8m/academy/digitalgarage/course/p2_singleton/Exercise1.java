package ch.ti8m.academy.digitalgarage.course.p2_singleton;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Exercise1 {


    public static void main(String[] args) {

        List<Integer> sequences = IntStream.rangeClosed(1, 40).boxed()
                .map(i -> FibonacciService.getInstance().getNext())
                .collect(Collectors.toList());

        System.out.println(sequences);

    }

}
