package ch.ti8m.academy.digitalgarage.course.p1_lambda;

import java.util.function.Function;

public class Example1 {


    public static void main(String[] args) {

        Function<Integer, Integer> incByOne = x -> x + 1;

        Integer result = incByOne.apply(10);

        System.out.println(result);

    }

}
