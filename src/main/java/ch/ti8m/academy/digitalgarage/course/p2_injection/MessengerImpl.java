package ch.ti8m.academy.digitalgarage.course.p2_injection;

public class MessengerImpl implements Messenger {

    @Override
    public boolean sendMessage(String message) {
        System.out.println(String.format("I just sent the message %s using MessengerImpl", message));
        return true;
    }

}
