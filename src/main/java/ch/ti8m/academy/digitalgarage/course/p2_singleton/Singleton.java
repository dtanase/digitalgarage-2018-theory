package ch.ti8m.academy.digitalgarage.course.p2_singleton;

public class Singleton {

    //create an object of Singleton
    private static Singleton instance = new Singleton();

    //make the constructor private so that this class cannot be instantiated
    private Singleton() {}

    //Get the only object available
    public static Singleton getInstance(){
        return instance;
    }

    public String getMessage(){
        return "Hello from Singleton!";
    }

}
