package ch.ti8m.academy.digitalgarage.course.util;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @Builder @AllArgsConstructor
public class Employee {

    private Integer id;

    private String name;

    private Integer salary;


    @Override
    public String toString() {
        return this.getName();
    }
}
