package ch.ti8m.academy.digitalgarage.course.p2_singleton;

public class LazySingleton {

    //initially, the instance is null
    private static LazySingleton instance;

    //make the constructor private so that this class cannot be instantiated
    private LazySingleton() {
    }

    //Get the only object available
    public static LazySingleton getInstance() {
        if (instance == null)
            instance = new LazySingleton();
        return instance;
    }

    public String getMessage() {
        return "Hello from Singleton!";
    }

}
