package ch.ti8m.academy.digitalgarage.course.p2_injection;

import javax.inject.Inject;

public class CommunicationService {

    @Inject
    private Messenger messenger;

    public CommunicationService() { }

    public boolean sendMessage(String message) {
        return messenger.sendMessage(message);
    }

}
