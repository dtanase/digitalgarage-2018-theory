package ch.ti8m.academy.digitalgarage.course.p1_streams;

import java.util.Arrays;
import java.util.List;

public class Exercise2 {

    public static void main(String[] args) {


        List<Integer> intList = Arrays.asList(2, 4, 5, 6, 8);

        boolean allMatch  = intList.stream().allMatch(i -> i % 2 == 0);
        boolean anyMatch  = intList.stream().anyMatch(i -> i % 2 == 0);
        boolean noneMatch = intList.stream().noneMatch(i -> i % 3 == 0);


        System.out.println(allMatch);
        System.out.println(anyMatch);
        System.out.println(noneMatch);

    }

}
