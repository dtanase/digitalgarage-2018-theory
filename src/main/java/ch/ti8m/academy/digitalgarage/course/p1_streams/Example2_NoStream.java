package ch.ti8m.academy.digitalgarage.course.p1_streams;

import ch.ti8m.academy.digitalgarage.course.util.Status;
import ch.ti8m.academy.digitalgarage.course.util.Transaction;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Example2_NoStream {

    public static void main(String[] args) {


        List<Transaction> transactions = Arrays.asList(
                Transaction.builder().id(1L).status(Status.WARNING).value(BigDecimal.valueOf(100.00)).build(),
                Transaction.builder().id(3L).status(Status.OK).value(BigDecimal.valueOf(80.00)).build(),
                Transaction.builder().id(6L).status(Status.OK).value(BigDecimal.valueOf(120.00)).build(),
                Transaction.builder().id(7L).status(Status.WARNING).value(BigDecimal.valueOf(40.00)).build(),
                Transaction.builder().id(10L).status(Status.OK).value(BigDecimal.valueOf(50.00)).build()
        );


        List<Transaction> filtered = new LinkedList<>();
        for (Transaction transaction : transactions) {
            if(transaction.getStatus().equals(Status.OK))
                filtered.add(transaction);
        }

        filtered.sort(Comparator.comparing(Transaction::getValue).reversed());


        List<Long> result = new LinkedList<>();

        for (Transaction transaction : filtered) {
            result.add(transaction.getId());
        }

        System.out.println(result);


    }


}
