package ch.ti8m.academy.digitalgarage.course.p2_injection;

public interface Messenger {

    boolean sendMessage(String message);

}
