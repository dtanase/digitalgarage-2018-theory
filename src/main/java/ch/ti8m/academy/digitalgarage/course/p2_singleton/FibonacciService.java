package ch.ti8m.academy.digitalgarage.course.p2_singleton;

public class FibonacciService {

    private static FibonacciService service = new FibonacciService();

    private FibonacciService() { }


    public static FibonacciService getInstance() {
        return service;
    }

    public int getNext() {
        return -1; //TODO: Implement here the code
    }

}
