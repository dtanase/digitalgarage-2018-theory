package ch.ti8m.academy.digitalgarage.course.p2_injection;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Example1 {

    public static void main(String[] args){
        Injector injector = Guice.createInjector(new CommunicationModule());

        CommunicationService comm = injector.getInstance(CommunicationService.class);
        comm.sendMessage("Hello Digital Garage 2018!");
    }

}
