package ch.ti8m.academy.digitalgarage.course.p2_annotation;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;

public class JsonSerializer {

    public String serialize(Object object) {

        Map<String, String> jsonElements = new HashMap<>();

        try {

            Class<?> objectClass = requireNonNull(object).getClass();

            for (Field field: objectClass.getDeclaredFields()) {
                field.setAccessible(true);
                if (field.isAnnotationPresent(JsonField.class)) {
                    Object obj = field.get(object);
                    if(obj != null) jsonElements.put(getSerializedKey(field), String.valueOf(obj));
                }
            }

            return toJsonString(jsonElements);

        }

        catch (IllegalAccessException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private String toJsonString(Map<String, String> jsonMap) {
        String elementsString = jsonMap.entrySet().stream()
                .map(entry -> "\""  + entry.getKey() + "\":\"" + entry.getValue() + "\"")
                .collect(joining(","));

        return "{" + elementsString + "}";
    }


    private static String getSerializedKey(Field field) {
        String annotationValue = field.getAnnotation(JsonField.class).value();
        if (annotationValue.isEmpty())
            return field.getName();
        else
            return annotationValue;
    }

}
