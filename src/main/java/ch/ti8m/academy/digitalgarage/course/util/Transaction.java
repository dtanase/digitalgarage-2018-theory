package ch.ti8m.academy.digitalgarage.course.util;

import ch.ti8m.academy.digitalgarage.course.p2_annotation.JsonField;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Getter @Setter @ToString @Builder
public class Transaction {

    @JsonField
    private Long id;

    @Builder.Default
    @JsonField
    private LocalDate date = LocalDate.now();

    @JsonField
    private Status status;

    @JsonField
    private BigDecimal value;

    @JsonField("booking_text")
    private String bookingText;

    @JsonField("customer_id")
    private Long customerId;

    @JsonField("credit_account")
    private String creditAccount;

    @JsonField("debit_account")
    private String debitAccount;

    @JsonField
    private String country;

    @Singular
    private List<String> attributes;

}